# Домашнее задание по модулю "Основы культуры CI 3: linters" курса "Python Advanced"

![pipline](https://gitlab.com/skillbox_edu/python/python_advanced/module_30/badges/main/pipeline.svg)

## Цели практической работы
* Научиться работать с линтерами: flake-8, isort, black, mypy.
* Научиться работать с gitlab-ci.

## Что нужно сделать
1. Перенесите код задания по FastAPI в новый репозиторий.
1. Настройте для этого репозитория gitlab-ci c линтерами и тестами.
1. Исправьте все ошибки от линтеров в коде.

## Установка
1. Клонируем репозиторий
   ```shell
   git clone https://gitlab.com/skillbox_edu/python/python_advanced/module_30.git
   cd module_30
   ```

1. Настраиваем виртуальное окружение для своей ОС и активируем его

1. Ставим зависимости
   ```shell
   python -m pip install --upgrade pip setuptools wheel
   python -m pip install -r ./conf/python/requirements.txt
   ```

1. Предварительно нужно создать файл `.env` рядом с [`docker-compose.yml`](docker-compose.yml). Пример файла: [`env.example`](env.example).
   
1. Запусаем контейнер
   ```shell
   docker-compose up -d
   ```
   
1. Можно сгенерировать наполнение для БД
   ```shell
   docker-compose exec api python gen_db.py
   ```
   
1. [Документация](http://127.0.0.1:5000/docs) по API

   ![fastapi docs](docs/imgs/fastapi-docs.png)

1. [Фронт](http://127.0.0.1:8080/) на Vue + Vuetify

   ![fornt](docs/imgs/front.png)

## Тестирование
```shell
docker-compose exec api pytest -v
```
   
## Полезные материалы
* [Async SQLAlchemy](https://docs.sqlalchemy.org/en/20/orm/extensions/asyncio.html#asyncio-platform-installation-notes-including-apple-m1)
* [FastAPI](https://fastapi.tiangolo.com/)
* [FastAPI Pagination](https://uriyyo-fastapi-pagination.netlify.app/)
* [Testing FastAPI](https://fastapi.tiangolo.com/tutorial/testing/)
* [Advanced testing FastAPI](https://fastapi.tiangolo.com/advanced/async-tests/)
* [Uvicorn](https://www.uvicorn.org/)
* [PostgreSQL image](https://hub.docker.com/_/postgres)
* [Connect to PostgreSQL with SQLAlchemy and asyncio](https://makimo.pl/blog/connect-to-postgresql-with-sqlalchemy-and-asyncio/)
* [VueJS 3.x](https://vuejs.org/guide/quick-start.html)
* [Vue + Fetch](https://jasonwatmore.com/post/2020/04/30/vue-fetch-http-get-request-examples)
* [The best UI frameworks for Vue 3](https://blog.logrocket.com/best-ui-frameworks-vue-3/)
* [Vuetify - Vue Component Framework](https://vuetifyjs.com/en/getting-started/installation/)
* [FastAPI Tips & Tricks: Testing a Database](https://dev.to/jbrocher/fastapi-testing-a-database-5ao5)
* [SQLAlchemy-Utils: Database helpers](https://sqlalchemy-utils.readthedocs.io/en/latest/database_helpers.html)
* [Gitlab services: Using PostgreSQL](https://docs.gitlab.com/ee/ci/services/postgres.html)
