from fastapi.middleware import Middleware
from fastapi.middleware.cors import CORSMiddleware

try:
    from .api import create_api
except ImportError:
    from api import create_api  # type: ignore[no-redef]

app = create_api(
    [
        Middleware(
            cls=CORSMiddleware,
            allow_origins=["*"],
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        ),
    ]
)
