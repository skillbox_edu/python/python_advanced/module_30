from typing import List

from pydantic import BaseModel, Field


class Error(BaseModel):
    detail: str


class Recipe(BaseModel):
    name: str = Field(..., title="название блюда", max_length=200)
    description: str = Field(..., title="текстовое описание")
    cooking_time: int = Field(default=1, title="время приготовления", ge=1)
    ingredients: List[str] = Field(..., title="список ингредиентов", min_items=1)


class RecipeIn(Recipe):
    ...


class RecipeOut(Recipe):
    id: int
    reviews: int

    class Config:
        orm_mode = True
