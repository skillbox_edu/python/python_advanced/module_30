import pytest as pytest
import sqlalchemy_utils as sautils
from fastapi.testclient import TestClient
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool

from ..api import create_api
from ..db.models import Base, Recipe, get_db
from ..db.pg import POSTGRES_HOST, POSTGRES_PASSWORD, POSTGRES_USER

SYNC_SCHEMA = "postgresql"
ASYNC_SCHEMA = f"{SYNC_SCHEMA}+asyncpg"
TEST_DATABASE_URL = f"://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}:5432/test_db"

TestSession = sessionmaker(expire_on_commit=False, class_=AsyncSession)


@pytest.fixture
async def app(db_session):
    _app = create_api()

    recipes = [
        Recipe(
            name="рецепт 1",
            description="описание рецепта 1",
            cooking_time=1,
            ingredients=[
                "ингридиент 1",
            ],
        ),
        Recipe(
            name="рецепт 2",
            description="описание рецепта 2",
            cooking_time=2,
            reviews=2,
            ingredients=[
                "ингридиент 1",
                "ингридиент 2",
            ],
        ),
        Recipe(
            name="рецепт 3",
            description="описание рецепта 3",
            cooking_time=3,
            reviews=3,
            ingredients=[
                "ингридиент 1",
                "ингридиент 2",
                "ингридиент 3",
            ],
        ),
    ]
    async with db_session.begin():
        db_session.add_all(recipes)

    yield _app


@pytest.fixture(scope="session")
def anyio_backend():
    return "asyncio"


@pytest.fixture(scope="session")
async def engine(anyio_backend):
    # создаем тестовую базу, если ее нет
    if not sautils.database_exists(SYNC_SCHEMA + TEST_DATABASE_URL):
        sautils.create_database(SYNC_SCHEMA + TEST_DATABASE_URL)

    test_engine = create_async_engine(
        ASYNC_SCHEMA + TEST_DATABASE_URL,
        echo=False,
        # https://docs.sqlalchemy.org/en/20/orm/extensions/asyncio.html#using-multiple-asyncio-event-loops
        poolclass=NullPool,
    )

    # создаем таблицы
    async with test_engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)

    yield test_engine

    # удаляем таблицы
    async with test_engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)

    await test_engine.dispose()


@pytest.fixture
async def db_session(engine):
    session = TestSession(bind=engine)
    yield session
    await session.close()


@pytest.fixture
def client(app, db_session):
    app.dependency_overrides[get_db] = lambda: db_session

    with TestClient(app) as test_client:
        yield test_client
