from collections.abc import Sequence
from typing import Optional

from fastapi import FastAPI
from fastapi.middleware import Middleware
from fastapi_pagination import add_pagination

try:
    from . import routers
except ImportError:
    import routers  # type: ignore[no-redef]


def create_api(middlewares: Optional[Sequence[Middleware]] = None) -> FastAPI:
    app = FastAPI(
        title="Recipe Book API",
        description="Recipe Book API",
        version="1.0.0",
        middleware=middlewares,
        lifespan=routers.lifespan,
    )

    # pagination
    add_pagination(routers.prefix_router)

    # routers
    app.include_router(routers.prefix_router)

    return app
