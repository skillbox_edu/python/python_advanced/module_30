from fastapi import Depends
from sqlalchemy import CheckConstraint, Column, Integer, String, Text
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import declarative_base, sessionmaker

from .pg import POSTGRES_DB, POSTGRES_HOST, POSTGRES_PASSWORD, POSTGRES_USER

DATABASE_URL = f"postgresql+asyncpg://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}:5432/{POSTGRES_DB}"

engine = create_async_engine(DATABASE_URL, echo=True)

Session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)
session = Session()

Base: type = declarative_base()


def get_db():
    session = Session()
    try:
        yield session
    finally:
        session.close()


depends = Depends(get_db)


class Recipe(Base):
    __tablename__ = "recipes"

    id = Column(Integer, primary_key=True)
    name = Column(String(length=200), nullable=False, doc="название блюда")
    description = Column(Text, nullable=False, doc="текстовое описание")
    cooking_time = Column(Integer, nullable=False, default=1, doc="время приготовления (мин)")
    reviews = Column(Integer, nullable=False, default=0, doc="количество просмотров")
    ingredients = Column(ARRAY(String), nullable=False, doc="список ингредиентов")

    __table_args__ = (
        CheckConstraint(reviews >= 0, name="reviews_positive_constraint"),
        CheckConstraint(cooking_time > 0, name="cooking_time_not_negative_constraint"),
        CheckConstraint(
            "COALESCE(array_length(ingredients, 1), 0) > 0", name="ingredients_length_not_negative_constraint"
        ),
    )


async def init_db():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
