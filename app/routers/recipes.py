from typing import List, Optional

import sqlalchemy
from fastapi import APIRouter, HTTPException
from fastapi_pagination import Page, paginate

try:
    from .. import schemas
    from ..db import models
except ImportError:
    import schemas  # type: ignore[no-redef]
    from db import models  # type: ignore[no-redef]

recipes_router = APIRouter(
    prefix="/recipes",
)


@recipes_router.get("/", summary="Получить список рецептов", response_model=Page[schemas.RecipeOut])
async def recipe(db: models.Session = models.depends) -> List[models.Recipe]:
    async with db.begin():
        recipes = await db.execute(
            sqlalchemy.select(models.Recipe).order_by(models.Recipe.reviews.desc(), models.Recipe.cooking_time)
        )
        return paginate(recipes.scalars().all())


@recipes_router.get(
    "/{id}",
    summary="Получить рецепт по id",
    response_model=schemas.RecipeOut,
    responses={
        404: {
            "model": schemas.Error,
            "description": "Not Found",
        }
    },
)
async def recipe_detail(id: int, db: models.Session = models.depends) -> models.Recipe:
    async with db.begin():
        result = await db.execute(sqlalchemy.select(models.Recipe).where(models.Recipe.id == id))
        recipe: Optional[models.Recipe] = result.scalars().one_or_none()

        if recipe is None:
            raise HTTPException(status_code=404, detail=f"Recipe with id {id} not found")
        else:
            recipe.reviews += 1
            await db.commit()

            return recipe


@recipes_router.post(
    "/",
    status_code=201,
    summary="Добавить рецепт",
    response_model=schemas.RecipeOut,
)
async def add_recipe(body: schemas.RecipeIn, db: models.Session = models.depends) -> models.Recipe:
    new_recipe = models.Recipe(**body.dict())
    async with db.begin():
        db.add(new_recipe)
    return new_recipe
