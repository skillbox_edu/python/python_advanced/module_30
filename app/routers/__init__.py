import contextlib

from fastapi import APIRouter, FastAPI

try:
    from ..db import models
except ImportError:
    from db import models  # type: ignore[no-redef]

from .recipes import recipes_router

prefix_router = APIRouter(prefix="/api/v1")
prefix_router.include_router(recipes_router)


@contextlib.asynccontextmanager
async def lifespan(app: FastAPI):
    await models.init_db()

    yield

    await models.session.close()
    await models.engine.dispose()
