#!/bin/bash

COLOR_GREEN='\e[0;32m'
COLOR_RED='\e[0;31m'
NO_COLOR='\e[0;0m'

isort --check --diff --color $1
isort_result=$?

black --check --diff --color $1
black_result=$?

flake8 --color always $1
flake8_result=$?

mypy --color-output $1
mypy_result=$?

if [[ isort_result -eq 0 && black_result -eq 0 && flake8_result -eq 0 && mypy_result -eq 0 ]]; then
    echo -e "${COLOR_GREEN}OK${NO_COLOR}"
    exit 0
else
    echo -e "${COLOR_RED}Fail${NO_COLOR}"
    exit 1
fi
